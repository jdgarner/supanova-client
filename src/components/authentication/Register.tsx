import { useState } from "react";
import toast from "react-hot-toast";
import {
  REACT_TOAST_DURATION,
  feedbackMessages,
} from "src/constants/constants";
import { useAuth } from "src/contexts/AuthContext";
import useRequest from "src/hooks/useRequest";
import { FormSubmitEvent, PasswordsShowing } from "src/types";

import AuthCard from "./AuthCard";
import PasswordVisibilityIcon from "./PasswordVisibilityIcon";
import WarningIcon from "../../assets/icons/warningIcon.svg?react";
import FormInput from "../FormInput";

const formGroupClassname = "mb-2 auth-form-input";

const Register = () => {
  const [emailInput, setEmailInput] = useState<string>("");
  const [passwordInput, setPasswordInput] = useState<string>("");
  const [nameInput, setNameInput] = useState<string>("");
  const [repeatPasswordInput, setRepeatPasswordInput] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isPasswordShowing, setIsPasswordShowing] = useState<PasswordsShowing>({
    password: false,
    repeatPassword: false,
  });

  const registerUser = useRequest("/register");
  const { login } = useAuth();

  const onError = (errorName: string, error: any) => {
    console.log(`${errorName}: ${error}`);
    if (error.code === "auth/email-already-in-use") {
      toast.error(feedbackMessages.accountAlreadyExists, REACT_TOAST_DURATION);
    } else {
      toast.error(feedbackMessages.registrationError, REACT_TOAST_DURATION);
    }
  };

  const onHandleRegisterUser = async (event: FormSubmitEvent) => {
    event.preventDefault();

    if (repeatPasswordInput !== passwordInput) {
      toast.error(feedbackMessages.passwordMismatch, {
        icon: <WarningIcon height="22px" width="22px" />,
        ...REACT_TOAST_DURATION,
      });
    } else {
      try {
        setIsLoading(true);

        registerUser({
          requestBody: {
            name: nameInput,
            email: emailInput,
            password: passwordInput,
          },
          onSuccess: () => login(emailInput, passwordInput),
          onError: error => onError("Create new user error", error),
        });
      } catch (createNewUserError) {
        onError("Create new user error", createNewUserError);
      }
    }

    setIsLoading(false);
  };

  const onTogglePasswordVisibility = (
    key: "password" | "repeatPassword",
    value: boolean,
  ) => {
    const updatedPasswordShowingState = {
      ...isPasswordShowing,
      [key]: value,
    };

    setIsPasswordShowing(updatedPasswordShowingState);
  };

  return (
    <AuthCard
      cardClassname="registration-card"
      logoClassname="mt-3"
      title="Welcome!"
      subTitle="Create your Supanova account"
      buttonText="Sign up"
      isLoading={isLoading}
      footerText="Have an account?"
      footerLinkText="Login"
      footerLinkPath="/login"
      onSubmit={onHandleRegisterUser}
    >
      <FormInput
        formId="username"
        formGroupClassname={formGroupClassname}
        inputContainerClassname="text-input"
        label="First name"
        type="text"
        value={nameInput}
        onChange={e => setNameInput(e.target.value)}
      />
      <FormInput
        formId="email"
        formGroupClassname={formGroupClassname}
        inputContainerClassname="text-input"
        label="Email"
        type="email"
        value={emailInput}
        onChange={e => setEmailInput(e.target.value)}
      />
      <FormInput
        formId="password"
        formGroupClassname={formGroupClassname}
        inputContainerClassname="d-flex align-items-center password-input"
        label="Password"
        type={isPasswordShowing.password ? "text" : "password"}
        value={passwordInput}
        onChange={e => setPasswordInput(e.target.value)}
        minLength={6}
        Component={
          <PasswordVisibilityIcon
            isPasswordShowing={isPasswordShowing.password}
            onTogglePasswordVisibility={value =>
              onTogglePasswordVisibility("password", value)
            }
          />
        }
      />
      <FormInput
        formId="repeat-password"
        formGroupClassname={formGroupClassname}
        inputContainerClassname="d-flex align-items-center password-input"
        label="Repeat password"
        type={isPasswordShowing.repeatPassword ? "text" : "password"}
        value={repeatPasswordInput}
        onChange={e => setRepeatPasswordInput(e.target.value)}
        minLength={6}
        Component={
          <PasswordVisibilityIcon
            isPasswordShowing={isPasswordShowing.repeatPassword}
            onTogglePasswordVisibility={value =>
              onTogglePasswordVisibility("repeatPassword", value)
            }
          />
        }
      />
    </AuthCard>
  );
};

export default Register;
