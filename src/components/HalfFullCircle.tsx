const HalfFullCircle = () => {
  return (
    <div className="circle-container">
      <div className="circle">
        <div className="left-half" />
        <div className="right-half" />
      </div>
    </div>
  );
};

export default HalfFullCircle;
