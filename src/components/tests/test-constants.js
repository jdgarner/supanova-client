export const defaultCourses = [
  {
    id: 1,
    title: "Managing Stakeholders",
    sections: [
      {
        id: 1,
        title: "Intro",
      },
      {
        id: 2,
        title: "Definitions",
      },
      {
        id: 3,
        title: "Important to note",
      },
      {
        id: 4,
        title: "Conclusion",
      },
    ],
  },
  {
    id: 2,
    title: "Radiation Basics",
    sections: [
      {
        id: 5,
        title: "Welcome!",
      },
      {
        id: 6,
        title: "Tools",
      },
      {
        id: 7,
        title: "Next steps",
      },
    ],
  },
];
