import { FormCheck, FormLabel } from "react-bootstrap";
import PlusIcon from "src/assets/icons/plusIcon.svg?react";
import FormInput from "src/components/FormInput";
import RemoveInput from "src/components/RemoveInput";
import { colors } from "src/constants/colorPalette";
import { CourseQuizAnswer } from "src/types";

interface QuizAnswersProps {
  answers: CourseQuizAnswer[];
  canRemoveQuizAnswer: boolean;
  onClickAddNewQuizAnswer: () => void;
  onClickRemoveQuizAnswer: (answerId: string) => void;
  onClickToggleCorrectQuizAnswer: (
    answerId: string,
    isCorrectAnswer: boolean,
  ) => void;
  onChangeQuizAnswer: (inputValue: string, answerId: string) => void;
}

const QuizAnswers: React.FC<QuizAnswersProps> = ({
  answers,
  canRemoveQuizAnswer,
  onChangeQuizAnswer,
  onClickAddNewQuizAnswer,
  onClickRemoveQuizAnswer,
  onClickToggleCorrectQuizAnswer,
}) => {
  return (
    <div>
      <FormLabel className="m-0">Answers (tick correct ones)</FormLabel>
      {answers.map(({ answer, id: answerId, isCorrectAnswer }) => {
        return (
          <div className="d-flex flex-column" key={`quiz-answer-${answerId}`}>
            <div className="quiz-checkbox">
              <FormCheck
                className="me-3"
                type="checkbox"
                checked={isCorrectAnswer}
                id={`quiz-checkbox-${answerId}`}
                onChange={() =>
                  onClickToggleCorrectQuizAnswer(answerId, isCorrectAnswer)
                }
              />
              <FormInput
                formId={`quiz-answer-${answerId}`}
                type="text"
                key={`quiz-answer-${answerId}`}
                value={answer}
                onChange={e => onChangeQuizAnswer(e.target.value, answerId)}
                formGroupClassname="my-4 section-input"
              />
              {canRemoveQuizAnswer ? (
                <RemoveInput
                  onClickFunction={() => onClickRemoveQuizAnswer(answerId)}
                  margin="ms-2 mb-3"
                  padding="pt-3 px-2"
                />
              ) : null}
            </div>
          </div>
        );
      })}
      <div
        className="quiz-plus-icon-container"
        onClick={onClickAddNewQuizAnswer}
      >
        <p className="m-0">
          <strong>Add quiz answer</strong>
        </p>

        <div className="quiz-plus-icon">
          <PlusIcon stroke={colors.green} className="mb-4" />
        </div>
      </div>
    </div>
  );
};

export default QuizAnswers;
