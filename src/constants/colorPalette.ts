export const colors = {
  green: "#198754",
  red: "#dc3545",
  orange: "#f18f0f",
  darkgrey: "#6c757d",
  lightgrey: "#a8a8a8",
  yellow: "#ffc007",
  blue: "#3397E8",
  white: "#FFFFFF",
};
